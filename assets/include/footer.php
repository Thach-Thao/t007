<footer class="c-footer">
	<div class="l-wrapper">
		<div class="c-footer__txt">
			(c) 2016 TokyomeganeCo.,Ltd. All Rights Reserved.
		</div>
	</div>
</footer>
<link rel="stylesheet" type="text/css" href="/assets/js/slick/slick.css">
<link rel="stylesheet" type="text/css" href="/assets/js/slick/slick-theme.css">

<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="/assets/js/slick/slick.js"></script>

<script src="/assets/js/functions.js"></script>
<script type="text/javascript">

	//slider
	$(document).ready(function(){
		$('.slider').slick({
			dots: true,
			autoplay: false,
			autoplaySpeed: 3000,
		});
	});

	$(function(){
		$('a[href^="#"]').click(function(){
			$('html,body').animate({scrollTop: $($(this).attr('href')).offset().top}, 800, 'swing');
			return false;
		});
	});

</script>

</body>
</html>