<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/meta.php'); ?>
<link href="/assets/css/style.css" rel="stylesheet">
<script src="/assets/js/jquery-1.12.4.min.js"></script>
<script src="/assets/js/jquery.matchHeight-min.js"></script>

<script type="text/javascript">
	$(function(){
		$('.animation').css('visibility','hidden');
		$(window).scroll(function(){
			var windowHeight = $(window).height(),
			topWindow = $(window).scrollTop();
			$('.animation').each(function(){
				var targetPosition = $(this).offset().top;
				if(topWindow > targetPosition - windowHeight + 100){
					$(this).addClass("fadeInUp");
				}
			});
		});


		setScroll();
		setVisible();
		$(window).scroll(function() {
			setScroll();
			setVisible();
		});
	});

	function setVisible() {
		var scroll = $(window).scrollTop();
		var off01 = $('#merit').offset().top;
	    if (scroll >= 630) {
	        $('.page-control').removeClass('hidden');
	    } else {
	        $('.page-control').addClass('hidden');
	    }
	}

	function setScroll(){
		var sTop = $(document).scrollTop();
		var winH = $(window).height();
		var off01 = $('#merit').offset().top;
		var off02 = $('#step').offset().top;
		var off03 = $('#report').offset().top;
		var off04 = $('#classroom').offset().top;
		var off05 = $('#access').offset().top;
		var off06 = $('#qa').offset().top;
		var off07 = $('#contact-form').offset().top;

		if(sTop+1 >=off01){
			$('.page-control li a').removeClass('page-control-active');
			$('.page-control li:eq(0) a').addClass('page-control-active');
		}

		if(sTop+1 >= off02){
			$('.page-control li a').removeClass('page-control-active');
			$('.page-control li:eq(1) a').addClass('page-control-active');
		}

		if(sTop+1 >= off03){
			$('.page-control li a').removeClass('page-control-active');
			$('.page-control li:eq(2) a').addClass('page-control-active');
		}

		if(sTop+1 >= off04){
			$('.page-control li a').removeClass('page-control-active');
			$('.page-control li:eq(3) a').addClass('page-control-active');
		}

		if(sTop+1 >= off05){
			$('.page-control li a').removeClass('page-control-active');
			$('.page-control li:eq(4) a').addClass('page-control-active');
		}

		if(sTop+1 >= off06){
			$('.page-control li a').removeClass('page-control-active');
			$('.page-control li:eq(5) a').addClass('page-control-active');
		}

		if(sTop+1 >= off07){
			$('.page-control li a').removeClass('page-control-active');
			$('.page-control li:eq(6) a').addClass('page-control-active');
		}
	}
</script> 

</head>
<body class="page-<?php echo $id; ?>">

<header>
	<div class="l-wrapper">
		<div class="c-header">
			<div class="c-header__logo">
				<p class="pc-only"><img src="/assets/image/common/h_logo.png" width="230" height="69" alt=""></p>
				<p class="sp-only"><img src="/assets/image/common/h_logoSP.png" width="166" height="50" alt=""></p>
			</div>
			<div class="sp-only c-menuSP">
				<div class="c-menuSP__icon" id="spBtn">
					<img src="/assets/image/common/icon_menuSP.png" alt="" id="menuBtn" width="40" height="38">
				</div>
				<div id="sp-menu" class="sp-only c-menu">
					<img class="c-menuSP__icon-close" src="/assets/image/common/icon_close.png" alt="" id="menuBtn" width="50" height="45">
					<ul class="c-menuSP__wrapper">
						<li class="c-menuSP__item"><a href="/">トップ</a></li>
						<li class="c-menuSP__item"><a href="">手作りのメリット</a></li>
						<li class="c-menuSP__item"><a href="">完成までのステップ</a></li>
						<li class="c-menuSP__item"><a href="">体験レポート</a></li>
						<li class="c-menuSP__item"><a href="">教室概要</a></li>
						<li class="c-menuSP__item"><a href="">アクセス</a></li>
						<li class="c-menuSP__item"><a href="">Q&amp;A</a></li>
						<li class="c-menuSP__item"><a href="">予約申し込み・お問い合わせ</a></li>
					</ul>
				</div>
			</div>

			<script type="text/javascript">
				$('#sp-menu').hide();
				$(function(){
					$("#spBtn").click(function() {
						$('#sp-menu').slideDown(600);
						$("body").addClass("is-open");
					});
					$(".c-menuSP__icon-close").click(function(e){
						$('#sp-menu').slideUp(600);
						$("body").removeClass("is-open");
					});
				});
			</script>

			<div class="c-header__centerImg">
				<img src="/assets/image/common/h_img01.png" width="459" height="459" alt="">
			</div>
			<div class="c-header__btn">
				<a href="#">
					ご予約・お問い合わせはこちら
				</a>
			</div>
		</div>
	</div>
</header>