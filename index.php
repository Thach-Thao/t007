<?php $id="index";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="p-index">
	<div class="page-control pc-only">
			<ul>
				<li>
					<a href="#merit" class="">
						<span>手作りのメリット</span>
				    </a>
			    </li>
			    <li>
					<a href="#step" class="">
					   <span>完成までのステップ</span>
				    </a>
			    </li>
			    <li>
					<a href="#report" class="">
					  <span>体験レポート</span>
				    </a>
			    </li>
			    <li>
					<a href="#classroom" class="">
					  <span>教室概要</span>
				    </a>
			    </li>
			    <li>
					<a href="#access" class="">
					 <span>アクセス</span>
				    </a>
			    </li>
			    <li>
					<a href="#qa" class="">
					  <span>Q&amp;A</span>
				    </a>
			    </li>
			    <li>
					<a href="#contact-form" class="">
					    <span>予約申し込み・お問い合わせ</span>
				    </a>
			    </li>
			</ul>
			<div class="page-control__line "></div>
	</div>

	<div class="p-index01">
		<div class="l-inner">
			<div class="p-index01__title">
				作れるのは、メガネだけではありません。
			</div>
			<div class="p-index01__txt01">
				好きなフレーム⽣地、デザインを選んで、<br class="sp-only">メガネを⼿作りする。<br>
				世界に一つだけのメガネとともに、<br class="sp-only">大切な思い出もでき上がります。
			</div>
			<div class="p-index01__txt02">
				オリジナルメガネとして愛⽤するのはもちろん、<br>テンプルにメッセージを⼊れて、贈り物にするのもおすすめです。
			</div>
		</div>

		<div id="merit" class="p-index01__cont">
			<div class="c-title01">
				<h2>手作りのメリット</h2>
			</div>
			<div class="c-block01">
				<div class="c-block01__item c-block01--left">
					<div class="c-block01__left">
						<div class="c-block01__img">
							<p class="pc-only"><img src="/assets/image/index/img01.png" width="960" height="396"></p>
							<p class="sp-only"><img src="/assets/image/index/img01_sp.png" width="610" height="441"></p>
						</div>
					</div>
					<div class="c-block01__right">
						<div class="c-block01__cont">
							<div class="c-block01__title">
								<div class="pc-only">世界に一つの<br>メガネ
								<span class="smtxt">
									MERIT <span class="u-color1">01</span>
								</span></div>
								<div class="sp-only">
									<span class="smtxt">
										MERIT <span class="u-color1">01</span>
									</span>
									<p>世界に一つのメガネ</p>
								</div>
							</div>
							<div class="c-block01__line01">
								<p class="pc-only"><img src="/assets/image/index/line01.png" width="573" height="14"></p>
							</div>
							<div class="c-block01__txt">
								自ら手作りしたメガネは世界に一つのメガネです。<br>
								しかも、自分の顔にピッタリ＆使い心地もバツグンのメガネです。<br>
								<span class="u-color1">メガネの専門家があなたに最適なフレームを素材選びから一緒にお探しします！</span>
							</div>
						</div>
					</div>
				</div>

				<div class="c-block01__item c-block01--right">
					<div class="c-block01__right">
						<div class="c-block01__img">
							<p class="sp-only"><img src="/assets/image/index/img02_sp.png" width="609" height="442"></p>
						</div>
					</div>
					<div class="c-block01__left">
						<div class="c-block01__cont">
							<div class="c-block01__title">
								<div class="pc-only">一生の<br>思い出に
								<span class="smtxt">
									MERIT <span class="u-color1">02</span>
								</span></div>
								<div class="sp-only">
									<span class="smtxt">
										MERIT <span class="u-color1">02</span>
									</span>
									<p>一生の思い出に</p>
								</div>
							</div>
							<div class="c-block01__line02">
								<p class="pc-only"><img src="/assets/image/index/line02.png" width="573" height="14"></p>
							</div>
							<div class="c-block01__txt">
								<span class="u-color1">「メガネを手作りする」<br>こんな体験はなかなかできるものではありません。</span><br>メガネとともに、かけがえのない思い出も作ることができます。
							</div>
						</div>
					</div>
					<div class="c-block01__right">
						<div class="c-block01__img">
							<p class="pc-only"><img src="/assets/image/index/img02.png" width="960" height="396"></p>
						</div>
					</div>
				</div>

				<div class="c-block01__item c-block01--left">
					<div class="c-block01__left">
						<div class="c-block01__img">
							<p class="pc-only"><img src="/assets/image/index/img03.png" width="960" height="394"></p>
							<p class="sp-only"><img src="/assets/image/index/img03_sp.png" width="610" height="440"></p>
						</div>
					</div>
					<div class="c-block01__right">
						<div class="c-block01__cont">
							<div class="c-block01__title">
								<div class="pc-only">プレゼントや<br>贈り物に
								<span class="smtxt">
									MERIT <span class="u-color1">03</span>
								</span></div>
								<div class="sp-only">
									<span class="smtxt">
										MERIT <span class="u-color1">03</span>
									</span>
									<p>プレゼントや贈り物に</p>
								</div>
							</div>
							<div class="c-block01__line01">
								<p class="pc-only"><img src="/assets/image/index/line01.png" width="573" height="14"></p>
							</div>
							<div class="c-block01__txt">
								世界に一つのオリジナルメガネを贈れば忘れられないプレゼントになります。<br>
								<span class="u-color1">テンプルにオリジナルメッセージを入れることもできます。</span>パートナー、家族、友達同士への、記念日の贈り物におすすめです。
							</div>
						</div>
					</div>
				</div>

				<div class="c-block01__item c-block01--right">
					<div class="c-block01__right">
						<div class="c-block01__img">
							<p class="sp-only"><img src="/assets/image/index/img04_sp.png" width="610" height="442"></p>
						</div>
					</div>
					<div class="c-block01__left">
						<div class="c-block01__cont">
							<div class="c-block01__title">
								<div class="pc-only">毎日使うもの<br>だから
								<span class="smtxt">
									MERIT <span class="u-color1">04</span>
								</span></div>
								<div class="sp-only">
									<span class="smtxt">
										MERIT <span class="u-color1">04</span>
									</span>
									<p>毎日使うものだから</p>
								</div>
							</div>
							<div class="c-block01__line02">
								<p class="pc-only"><img src="/assets/image/index/line02.png" width="573" height="14"></p>
							</div>
							<div class="c-block01__txt">
								<span class="u-color1">自分に合った、自分だけのメガネを自らの手で作ることができます。</span><br>毎日使うものだからこそ、愛着が持てるメガネを作っていただきたいのです。
							</div>
						</div>
					</div>
					<div class="c-block01__right">
						<div class="c-block01__img">
							<p class="pc-only"><img src="/assets/image/index/img04.png" width="960" height="396"></p>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

		<!----c-infor------->

		<div class="c-infor">
			<div class="c-infor__btn">
				<a href="#">ご予約・お問い合わせ</a>
			</div>
			<div class="c-infor__phone">
				<div class="pc-only">
					<div class="c-infor__colLeft">
						<p class="c-infor__num">03-3661-2906</p>
						<p class="c-infor__txt">
							日本橋人形町店（10:00-19:00）
						</p>
					</div>
					<div class="c-infor__colRight">
						<p class="c-infor__num">042-648-2311</p>
						<p class="c-infor__txt">
							八王子店（9:30-19:00）
						</p>
					</div>
				</div>
				<div class="sp-only">
					<a href=""><img src="/assets/image/common/info_sp.png" width="580" height="100"></a>
					<a href=""><img src="/assets/image/common/info_sp01.png" width="580" height="80"></a>
					<a href=""><img src="/assets/image/common/info_sp02.png" width="580" height="80"></a>
				</div>
			</div>
		</div>

		<!----p-index02------->

		<div id="step" class="p-index02">
			<div class="c-title01">
				<h2>完成までのステップ</h2>
			</div>
			<div class="p-index02__cont">
				<div class="p-index02__bg01 pc-only"></div>
				<div class="p-index02__left">
					<div class="p-step p-step--01">
						<span>体験</span>してみる
					</div>
					<div class="p-index02__item">
						<div class="p-index02__img">
							<img src="/assets/image/index/img05.png" width="960" height="430">
						</div>
						<div class="c-box">
							<div class="c-box__step">
								STEP<span>01</span>
							</div>
							<div class="c-box__ttl">
								フレーム選び
							</div>
							<div class="c-box__cont">
								<span>150種以上</span>の生地からフレーム素材を選択。<br>
								フロント、テンプルそれぞれに、馴染みやすい色・柄を選ぶか、あえてまったく異なるタイプを選ぶか、個性の見せどころです！
							</div>
						</div>
					</div>

					<div class="p-index02__item">
						<div class="p-index02__img">
							<img src="/assets/image/index/img06.png" width="960" height="430">
						</div>
						<div class="c-box">
							<div class="c-box__step">
								STEP<span>02</span>
							</div>
							<div class="c-box__ttl">
								デザイン選び
							</div>
							<div class="c-box__cont">
								<span>50種以上</span>のパターンの中から選びます。<br>
								選択に迷う場合は、メガネのスペシャリストであるスタッフが、お顔立ちにお似合いのデザインをおすすめします。
							</div>
						</div>
					</div>

					<div class="p-index02__item">
						<div class="p-index02__img">
							<img src="/assets/image/index/img07.png" width="960" height="430">
						</div>
						<div class="c-box">
							<div class="c-box__step">
								STEP<span>03</span>
							</div>
							<div class="c-box__ttl">
								フレームの切り出し
							</div>
							<div class="c-box__cont">
								パターン紙を生地に貼り付け、糸のこぎりで切り出します。<br>
								最初は少し難しさを感じるかもしれませんが、<span>コツをつかめば、スムーズに作業が進められるようになります。</span><br>
								スタッフもサポートいたしますので、ご安心ください。
							</div>
						</div>
					</div>

					<div class="p-index02__item">
						<div class="p-index02__img">
							<img src="/assets/image/index/img08.png" width="960" height="430">
						</div>
						<div class="c-box">
							<div class="c-box__step">
								STEP<span>04</span>
							</div>
							<div class="c-box__ttl">
								整形
							</div>
							<div class="c-box__cont">
								ヤスリとサンドペーパーを使って、切り出したフレームの形を整えます。
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="p-index02__cont01">
				<div class="p-index02__bg02 pc-only"></div>
				<div class="p-index02__right">
					<div class="p-step p-step--02">
						<span>職人</span>の仕上げ
					</div>
					<div class="p-index02__item item1">
						<div class="p-index02__img sp-only">
							<img src="/assets/image/index/img09.png" width="960" height="430">
						</div>
						<div class="c-box">
							<div class="c-box__step">
								STEP<span>05</span>
							</div>
							<div class="c-box__ttl">
								最終仕上げ
							</div>
							<div class="c-box__cont">
								作業が完了したフレーム(写真の状態のフレーム)を当店より福井県<span>鯖江市のメガネ職人に送付。</span>職人の手により、完成させます。
							</div>
						</div>
						<div class="p-index02__img pc-only">
							<img src="/assets/image/index/img09.png" width="960" height="430">
						</div>
					</div>
					<div class="p-index02__item">
						<div class="c-box">
							<div  class="sp-only">
								<p><img src="/assets/image/index/img11.png" width="727" height="189"></p>
							</div>
							<div class="c-box__step">
								STEP<span>06</span>
							</div>
							<div class="c-box__ttl">
								完成 !
							</div>
							<div class="c-box__cont">
								作業完了後、約1ヶ月半で<span>フレーム完成。</span><br>当店にてお渡し・郵送のご要望に応じて、お届けいたします。
							</div>
						</div>
						<div class="p-index02__img ">
							<p class="img01"><img src="/assets/image/index/img10.png" width="638" height="342"></p>
							<p class="img02 pc-only"><img src="/assets/image/index/img11.png" width="727" height="189"></p>
						</div>
					</div>
				</div>

			</div>
		</div>

		<!----p-video------->

		<div class="p-video">
			<p class="p-video__icon"><img src="/assets/image/index/icon01.png" width="50" height="18"></p>
			<div class="p-video__txt01">動画で見る</div>
			<div class="p-video__txt02">完成までのステップ</div>
			<div class="p-video__btnPlay">
				<img src="/assets/image/index/btnPlay.png" width="100" height="100">
			</div>
		</div>

		<!----c-infor------->

		<div class="c-infor">
			<div class="c-infor__btn">
				<a href="#">ご予約・お問い合わせ</a>
			</div>
			<div class="c-infor__phone">
				<div class="pc-only">
					<div class="c-infor__colLeft">
						<p class="c-infor__num">03-3661-2906</p>
						<p class="c-infor__txt">
							日本橋人形町店（10:00-19:00）
						</p>
					</div>
					<div class="c-infor__colRight">
						<p class="c-infor__num">042-648-2311</p>
						<p class="c-infor__txt">
							八王子店（9:30-19:00）
						</p>
					</div>
				</div>
				<div class="sp-only">
					<a href=""><img src="/assets/image/common/info_sp.png" width="580" height="100"></a>
					<a href=""><img src="/assets/image/common/info_sp01.png" width="580" height="80"></a>
					<a href=""><img src="/assets/image/common/info_sp02.png" width="580" height="80"></a>
				</div>
			</div>
		</div>

		<!----p-index03------->
		<div id="report" class="p-index03">
			 <div class="c-title01">
				<h2>体験レポート</h2>
			</div>

			<div class="p-slide">
				<div class="slider">
					<div class="p-slide__cont">
						<div class="slide-title">
							<p class="slide-title__txt01">CASE <span>01</span></p>
							<h3 class="slide-title__txt02">結婚式でのサプライズプレゼントに！</h3>
						</div>
						<div class="slide-content">
							<div class="slide-content__right">
								<img src="/assets/image/index/img12.png" width="400" height="240">
							</div>
							<div class="slide-content__left">
								<p class="u-mb40">メガネを手作りできるとは思っていませんでした。<br>
								作るときも楽しくて。</p>

								<p class="u-mb40">不器用なので、形になるのかと心配でしたが、出来上がりにビックリ！<br>
								売っているメガネのようで、使えるメガネになっていて嬉しかったです。</p>

								<p class="u-f16">日本橋人形町店にて参加<span class="pc-only">　　|　　</span><br class="sp-only">◯◯◯◯さん</p>
							</div>
						</div>
					</div>
					<div class="p-slide__cont"></div>
					<div class="p-slide__cont"></div>
				</div>
			</div>
		</div>

		<!----p-index04------->
		<div id="classroom" class="p-index04">
			<div class="c-title01">
				<h2>教室概要</h2>
			</div>
			<div class="p-index04__tbl">
				<div class="tbl-row">
					<div class="tbl-col1">
						開催店舗
					</div>
					<div class="tbl-col2">
						東京メガネ  日本橋人形町店、八王子店<br>※それぞれの店舗へのアクセスは<a href="#access">こちら</a>
					</div>
				</div>
				<div class="tbl-row">
					<div class="tbl-col1">
						開催日
					</div>
					<div class="tbl-col2">
						日本橋人形町店：随時開催(日曜定休)<br>
						八王子店：毎週土日<br>
						※事前予約が必要です。<a href="#contact-form">予約フォーム</a>からご予約下さい。
					</div>
				</div>
				<div class="tbl-row">
					<div class="tbl-col1">
						時 間
					</div>
					<div class="tbl-col2">
						10：00～17：00（13：00～14：00は休憩時間）<br>※作業時間の目安は5～7時間。数日に分けて作業していただくことも可能です。
					</div>
				</div>
				<div class="tbl-row">
					<div class="tbl-col1">
						定 員
					</div>
					<div class="tbl-col2">
						1日あたり2組（1組の人数：1～3名）
					</div>
				</div>
				<div class="tbl-row">
					<div class="tbl-col1">
						費 用
					</div>
					<div class="tbl-col2">
						21,600円（税込）※レンズは別売りです。 ※文字入れは別途有料です（540円～）
					</div>
				</div>
				<div class="tbl-row">
					<div class="tbl-col1">
						注意事項
					</div>
					<div class="tbl-col2">
						八王子店ではフレーム全体(フロント、テンプルとも)を手作りいただけます。<br>日本橋人形町店ではフロントは手作り、テンプルは既製のものからお選びいただきます。
					</div>
				</div>
			</div>
		</div>

		<!----c-infor------->

		<div class="c-infor">
			<div class="c-infor__btn">
				<a href="#">ご予約・お問い合わせ</a>
			</div>
			<div class="c-infor__phone">
				<div class="pc-only">
					<div class="c-infor__colLeft">
						<p class="c-infor__num">03-3661-2906</p>
						<p class="c-infor__txt">
							日本橋人形町店（10:00-19:00）
						</p>
					</div>
					<div class="c-infor__colRight">
						<p class="c-infor__num">042-648-2311</p>
						<p class="c-infor__txt">
							八王子店（9:30-19:00）
						</p>
					</div>
				</div>
				<div class="sp-only">
					<a href=""><img src="/assets/image/common/info_sp.png" width="580" height="100"></a>
					<a href=""><img src="/assets/image/common/info_sp01.png" width="580" height="80"></a>
					<a href=""><img src="/assets/image/common/info_sp02.png" width="580" height="80"></a>
				</div>
			</div>
		</div>

		<!----p-access------->

		<div id="access" class="p-access">
			<div class="c-title01">
				<h2>アクセス</h2>
			</div>
			<div class="p-access__content">
				<div class="p-access__item">
					<div class="p-access__map pc-only">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3240.7022602762027!2d139.78152161574084!3d35.68433258019356!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018894ffa9b29c3%3A0x1766f288016a1e1e!2zTmjhuq10IELhuqNuLCDjgJIxMDMtMDAxMyBUxY1recWNLXRvLCBDaMWrxY0ta3UsIE5paG9uYmFzaGluaW5necWNY2jFjSwgMSBDaG9tZeKIkjE14oiSNg!5e0!3m2!1svi!2s!4v1529637237807" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
					<div class="p-access__infor">
						<div class="infor-detail">
							<div class="infor-detail__title">
								日本橋人形町店
							</div><hr class="pc-only">
							<div class="p-access__map sp-only">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3240.7022602762027!2d139.78152161574084!3d35.68433258019356!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018894ffa9b29c3%3A0x1766f288016a1e1e!2zTmjhuq10IELhuqNuLCDjgJIxMDMtMDAxMyBUxY1recWNLXRvLCBDaMWrxY0ta3UsIE5paG9uYmFzaGluaW5necWNY2jFjSwgMSBDaG9tZeKIkjE14oiSNg!5e0!3m2!1svi!2s!4v1529637237807" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
							<ul class="infor-detail__list">
								<li>
									<span>住所</span>
									<p>東京都中央区日本橋人形町1-15-6</p>
								</li>
								<li>
									<span>TEL</span>
									<p>03-3661-2906</p>
								</li>
								<li>
									<span>営業時間</span>
									<p>10：00～19：00（祝日は18：00閉店）</p>
								</li>
								<li>
									<span>定休日</span>
									<p>日曜日</p>
								</li>
							</ul>
						</div>
					</div>
				</div>

				<hr class="sp-only" style="margin: 0 20px;">

				<div class="p-access__item">
					<div class="p-access__infor">
						<div class="infor-detail item2">
							<div class="infor-detail__title">
								八王子店
							</div><hr class="pc-only">
							<div class="p-access__map sp-only">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.811240444254!2d139.3377563157402!3d35.657021980199985!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60191ddeb52ed1cb%3A0xf628165461105588!2sYamaguchi+Ophthalmology+Clinic!5e0!3m2!1svi!2s!4v1529640429215" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>

							<ul class="infor-detail__list">
								<li>
									<span>住所</span>
									<p>東京都八王子市旭町10-13<br>カネダイビル5階</p>
								</li>
								<li>
									<span>TEL</span>
									<p>042-648-2311</p>
								</li>
								<li>
									<span>営業時間</span>
									<p>9：30～19：00</p>
								</li>
								<li>
									<span>定休日</span>
									<p>なし</p>
								</li>
							</ul>
						</div>
					</div>
					<div class="p-access__map pc-only">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.811240444254!2d139.3377563157402!3d35.657021980199985!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60191ddeb52ed1cb%3A0xf628165461105588!2sYamaguchi+Ophthalmology+Clinic!5e0!3m2!1svi!2s!4v1529640429215" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>

		<!----QA------->

		<div id="qa" class="p-qa">
			<div class="c-title01">
				<h2>Q&A</h2>
			</div>
			<div class="qa-content">
				<div class="qa-box">
					<div class="qa-box__question">
						<span>Q1.</span>
						<p>参加したいけれど、所要時間5～7時間は少し長くて……</p>
					</div>
					<div class="qa-box__answer">
						<span>A1.</span>
						<p>日を分割して作業していただくこともできます(その場合も費用は同じです)。ご予約の際にご相談ください。</p>
					</div>
				</div>
				<div class="qa-box">
					<div class="qa-box__question">
						<span>Q2.</span>
						<p>フレームが完成したら、レンズも選べるの？</p>
					</div>
					<div class="qa-box__answer">
						<span>A2.</span>
						<p>レンズは別売りとなりますが、もちろん当店にてレンズをお買い求めいただけます。<br>
						きめ細かい視力測定を経て、お客様の生活スタイルに最適なレンズを当店スタッフがご紹介いたします。</p>
					</div>
				</div>
				<div class="qa-box">
					<div class="qa-box__question">
						<span>Q3.</span>
						<p>自分用ではなく、サプライズで家族へのプレゼントとして手作りしたいのですが……</p>
					</div>
					<div class="qa-box__answer">
						<span>A3.</span>
						<p>お顔立ちに合わせたサイズのフレームを作成いたしますので、ご本人様におこしいただくのがベストですが、プレゼントされたいお相手がご愛用のフレームをお持ちになって、そちらを参考に手作りされたお客様もこれまでにいらっしゃいます。<br>詳細は、当店スタッフまでお問合せ・ご相談くださいませ。</p>
					</div>
				</div>
			</div>
		</div>

		<!----contact------->

		<div id="contact-form" class="p-contact">
			<div class="c-title01">
				<h2>手作りメガネ教室<br>予約申し込み・<br class="sp-only">お問い合わせフォーム</h2>
			</div>
			<form method="">
				<div class="contact-form">
					<div class="contact-row">
						<div class="col1">
							<p>お名前</p>
							<span>必須</span>
						</div>
						<div class="col2">
							<input class="size1" type="text" name="" placeholder="例）山田　太郎">
						</div>
					</div>
					<div class="contact-row">
						<div class="col1">
							<p>年齢</p>
							<span class="u-grey">任意</span>
						</div>
						<div class="col2">
							<input class="size2" type="text" name="">歳
						</div>
					</div>
					<div class="contact-row">
						<div class="col1">
							<p>性別</p>
							<span>必須</span>
						</div>
						<div class="col2">
							<span class="rdbtn"><input type="radio" name="" value="">男性</span>
	  						<span><input type="radio" name="" value="">女性</span>
						</div>
					</div>
					<div class="contact-row">
						<div class="col1">
							<p>お電話番号</p>
							<span>必須</span>
						</div>
						<div class="col2">
							<input class="size1" type="text" name="" placeholder="例）00-0000-0000">
						</div>
					</div>
					<div class="contact-row">
						<div class="col1">
							<p>メールアドレス</p>
							<span>必須</span>
						</div>
						<div class="col2">
							<input class="size1" type="text" name="" placeholder="例）sample@sample.com">
						</div>
					</div>
					<div class="contact-row">
						<div class="col1">
							<p>ご予約店舗</p>
							<span>必須</span>
						</div>
						<div class="col2">
							<span class="rdbtn"><input type="radio" name="" value="">日本橋人形町店</span><br class="sp-only">
	  						<span><input type="radio" name="" value="">八王子店</span>
						</div>
					</div>
					<div class="contact-row">
						<div class="col1">
							<p>ご希望日時 </p>
							<span>必須</span>
						</div>
						<div class="col2">
							日本橋人形町店<br class="sp-only">
							<select class="size3 pc-only">
								<option value="">--</option>
								<option value="">1</option>
								<option value="">2</option>
								<option value="">3</option>
								<option value="">4</option>
								<option value="">5</option>
								<option value="">6</option>
								<option value="">7</option>
								<option value="">8</option>
								<option value="">9</option>
								<option value="">10</option>
								<option value="">11</option>
								<option value="">12</option>
							</select>
							<select class="size3 sp-only">
								<option value="">月</option>
								<option value="">1</option>
								<option value="">2</option>
								<option value="">3</option>
								<option value="">4</option>
								<option value="">5</option>
								<option value="">6</option>
								<option value="">7</option>
								<option value="">8</option>
								<option value="">9</option>
								<option value="">10</option>
								<option value="">11</option>
								<option value="">12</option>
							</select>
							<span class="pc-only">月</span>
							<select class="size3 pc-only">
								<option value="">--</option>
								<option value="">1</option>
								<option value="">2</option>
								<option value="">3</option>
								<option value="">4</option>
								<option value="">5</option>
								<option value="">6</option>
								<option value="">7</option>
								<option value="">8</option>
								<option value="">9</option>
								<option value="">10</option>
								<option value="">11</option>
								<option value="">12</option>
								<option value="">13</option>
								<option value="">14</option>
								<option value="">15</option>
								<option value="">16</option>
								<option value="">17</option>
								<option value="">18</option>
								<option value="">19</option>
								<option value="">20</option>
								<option value="">21</option>
								<option value="">22</option>
								<option value="">23</option>
								<option value="">24</option>
								<option value="">25</option>
								<option value="">26</option>
								<option value="">27</option>
								<option value="">28</option>
								<option value="">29</option>
								<option value="">30</option>
								<option value="">31</option>
							</select>
							<select class="size3 sp-only">
								<option value="">日</option>
								<option value="">1</option>
								<option value="">2</option>
								<option value="">3</option>
								<option value="">4</option>
								<option value="">5</option>
								<option value="">6</option>
								<option value="">7</option>
								<option value="">8</option>
								<option value="">9</option>
								<option value="">10</option>
								<option value="">11</option>
								<option value="">12</option>
								<option value="">13</option>
								<option value="">14</option>
								<option value="">15</option>
								<option value="">16</option>
								<option value="">17</option>
								<option value="">18</option>
								<option value="">19</option>
								<option value="">20</option>
								<option value="">21</option>
								<option value="">22</option>
								<option value="">23</option>
								<option value="">24</option>
								<option value="">25</option>
								<option value="">26</option>
								<option value="">27</option>
								<option value="">28</option>
								<option value="">29</option>
								<option value="">30</option>
								<option value="">31</option>
							</select>
							<span class="pc-only">日</span>
							<select class="size3 pc-only">
								<option value="">--</option>
								<option value="">2018</option>
								<option value="">2017</option>
								<option value="">2016</option>
							</select>
							<select class="size3 sp-only">
								<option value="">時</option>
								<option value="">2018</option>
								<option value="">2017</option>
								<option value="">2016</option>
							</select>
							<span class="pc-only">時</span>

						</div>
					</div>
					<div class="contact-row">
						<div class="col1">
							<p>ご参加予定人数</p>
							<span>必須</span>
						</div>
						<div class="col2">
							<input class="size2" type="text" name="">人
						</div>
					</div>
					<div class="contact-row">
						<div class="col1-last">
							<p>その他</p>
							<span class="u-grey">任意</span>
						</div>
						<div class="col2">
							<div class="col2-last">
								<p>その他</p>
								<span class="u-grey">任意</span>
							</div>
							<textarea placeholder="その他ご不明点、ご要望など、どんなことでもご記入ください。"></textarea>
						</div>
					</div>
				</div>
			</form>
			<div class="frm-btn">
				<a href="">入力内容を確認する</a>
			</div>
		</div>

</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>